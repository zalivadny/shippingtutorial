# Release Notes for PluginShippingTutorial

## v0.1.2 (2019-11-28)

### Added
- image path fixed
- plugin type changed
- route check

## v0.1.1 (2019-11-26)

### Added
- template view test
- edited description & config

## v0.1.0 (2016-02-19)

### Added
- initial version