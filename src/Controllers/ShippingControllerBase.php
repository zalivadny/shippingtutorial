<?php

namespace ShippingTutorial\Controllers;

use Plenty\Modules\Account\Address\Contracts\AddressRepositoryContract;
use Plenty\Modules\Account\Address\Models\Address;
use Plenty\Modules\Cloud\Storage\Models\StorageObject;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Models\Order;
use Plenty\Modules\Order\Shipping\Contracts\ParcelServicePresetRepositoryContract;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Models\OrderShippingPackage;
use Plenty\Modules\Order\Shipping\PackageType\Contracts\ShippingPackageTypeRepositoryContract;
use Plenty\Modules\Order\Shipping\PackageType\Models\ShippingPackageType;
use Plenty\Modules\Order\Shipping\ParcelService\Models\ParcelServicePreset;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\ConfigRepository;

/**
 * Class ShippingControllerBase
 * @package ShippingTutorial\Controllers
 */
class ShippingControllerBase extends Controller
{
    /** @var Request */
    private $request;

    /** @var OrderRepositoryContract $orderRepository */
    private $orderRepository;

    /** @var AddressRepositoryContract $addressRepository */
    private $addressRepository;

    /** @var OrderShippingPackageRepositoryContract $orderShippingPackage */
    private $orderShippingPackage;

    /** @var ShippingInformationRepositoryContract */
    private $shippingInformationRepositoryContract;

    /** @var StorageRepositoryContract $storageRepository */
    private $storageRepository;

    /** @var ShippingPackageTypeRepositoryContract */
    private $shippingPackageTypeRepositoryContract;

    /** @var  array */
    private $createOrderResult = [];

    /** @var ConfigRepository */
    private $config;

    /** @var array $services */
    private $services = array(
        'TRCK' => 'SpringGDS TRACKED',
        'UNTR' => 'SpringGDS UNTRACKED',
        'PPBNT' => 'SpringGDS Packet Boxable Non Tracked',
        'PPTR' => 'SpringGDS Packet Registered',
        'PPTT' => 'SpringGDS Packet Tracked',
        'PPBTT' => 'SpringGDS Packet Boxable Tracked',
    );

    /**
     * ShippingControllerUntr constructor.
     * @param Request $request
     * @param OrderRepositoryContract $orderRepository
     * @param AddressRepositoryContract $addressRepositoryContract
     * @param OrderShippingPackageRepositoryContract $orderShippingPackage
     * @param StorageRepositoryContract $storageRepository
     * @param ShippingInformationRepositoryContract $shippingInformationRepositoryContract
     * @param ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract
     * @param ConfigRepository $config
     */
    public function __construct(Request $request,
                                OrderRepositoryContract $orderRepository,
                                AddressRepositoryContract $addressRepositoryContract,
                                OrderShippingPackageRepositoryContract $orderShippingPackage,
                                StorageRepositoryContract $storageRepository,
                                ShippingInformationRepositoryContract $shippingInformationRepositoryContract,
                                ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract,
                                ConfigRepository $config)
    {
        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->addressRepository = $addressRepositoryContract;
        $this->orderShippingPackage = $orderShippingPackage;
        $this->storageRepository = $storageRepository;

        $this->shippingInformationRepositoryContract = $shippingInformationRepositoryContract;
        $this->shippingPackageTypeRepositoryContract = $shippingPackageTypeRepositoryContract;

        $this->config = $config;
    }

    /**
     * Registers shipment(s)
     * @param Request $request
     * @param array $orderIds
     * @param string $service
     * @return array
     */
    public function registerShipmentsBase(Request $request, $orderIds = [], $service = 'UNTR'): array
    {
        $orderIds = $this->getOpenOrderIds($this->getOrderIds($request, $orderIds));
        $shipmentDate = date('Y-m-d');

        foreach ($orderIds as $orderId) {
            /** @var Order $order */
            $order = $this->orderRepository->findOrderById($orderId);
            /** @var Address $address */
            $address = $order->deliveryAddress;

            // gets order shipping packages from current order
            /** @var OrderShippingPackage $packages */
            $packages = $this->orderShippingPackage->listOrderShippingPackages($order->id);

            // iterating through packages
            foreach ($packages as $package) {
                $weight = $package->weight;
                /** @var ShippingPackageType $packageType */
                $packageType = $this->shippingPackageTypeRepositoryContract
                    ->findShippingPackageTypeById($package->packageId);

                try {
                    // todo : units
                    foreach ($order->orderItems as &$item) {
                        $item = (object)array(
                            'Description' => $item->orderItemName,
                            'Sku' => '',
                            'HsCode' => '',
                            'OriginCountry' => '',
                            'PurchaseUrl' => '',
                            'Quantity' => $item->quantity,
                            'Value' => '',
                        );
                    }

                    $shipment = json_encode(
                        array(
                            'Apikey' => $this->config->get('ShippingTutorial.apiKey'),
                            "Command" => 'OrderShipment',
                            'Shipment' => array(
                                "LabelFormat" => $this->config->get('ShippingTutorial.format', 'PDF'),
                                "ShipperReference" => $order->id . "-" . $order->plentyId, // unique
                                "Service" => $service,
                                "ConsignorAddress" => array(
                                    "Name" => $this->config->get('ShippingTutorial.senderName'),
                                    "Company" => $this->config->get('ShippingTutorial.companyName'),
                                    "AddressLine1" => $this->config->get('ShippingTutorial.senderStreet') . ' ' . $this->config->get('ShippingTutorial.senderNo'),
                                    "AddressLine2" => "",
                                    "City" => $this->config->get('ShippingTutorial.senderTown'),
                                    "State" => $this->config->get('ShippingTutorial.senderState'),
                                    "Zip" => $this->config->get('ShippingTutorial.senderPostalCode'),
                                    "Country" => $this->config->get('ShippingTutorial.senderCountryIso'), // iso
                                    "Phone" => $this->config->get('ShippingTutorial.senderPhone'),
                                    "Email" => $this->config->get('ShippingTutorial.senderEmail'),
                                ),
                                "ConsigneeAddress" => array(
                                    "Name" => $address->firstName . " " . $address->lastName,
                                    "Company" => $address->companyName,
                                    "AddressLine1" => $address->address1,
                                    "AddressLine2" => $address->address2,
                                    "City" => $address->town,
                                    "State" => $address->state,
                                    "Zip" => $address->postalCode,
                                    "Country" => $address->country->isoCode2,
                                    "Phone" => (!empty(trim($address->phone)) ? $address->phone : '0000000000'),
                                    "Email" => $address->email,
                                    "Vat" => '',
                                ),
                                "Length" => $packageType->length,
                                "Width" => $packageType->width,
                                "Weight" => $weight / 1000,
                                "Height" => $packageType->height,
                                "WeightUnit" => "kg",
                                "DimUnit" => "cm",
                                "Value" => $order->amounts[0]->invoiceTotal,
                                "Currency" => $order->amounts[0]->currency,
                                "DeclarationType" => $this->config->get('ShippingTutorial.declarationType'),
                                "Products" => $order->orderItems,
                                'Source' => 'plentymarkets',
                            )
                        )
                    );

                    $ch = curl_init(
                        'https://mtapi.net/'
                        . ($this->config->get('ShippingTutorial.mode', '0') === '0' ? '?testMode=1' : '')
                    );
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $shipment);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $response = json_decode(curl_exec($ch));
                    curl_close($ch);

                    $storageObject = $this->storageRepository->uploadObject(
                        'ShippingTutorial',
                        $response->Shipment->TrackingNumber . '.pdf',
                        base64_decode($response->Shipment->LabelImage),
                        true
                    );

                    $labelUrl = $this->storageRepository->getObjectUrl(
                        'ShippingTutorial',
                        $storageObject->key,
                        true,
                        3600
                    );

                    $shipmentItems = array([
                        'labelUrl' => $labelUrl,
                        'shipmentNumber' => $response->Shipment->TrackingNumber,
                    ]);

                    $this->orderShippingPackage->updateOrderShippingPackage(
                        $package->id,
                        [
                            'packageNumber' => $response->Shipment->TrackingNumber,
                            'label' => $storageObject->key,
//                            'labelPath' => $response->Shipment->TrackingNumber . '.pdf',
                        ]
                    );

                    // adds result UD004894627NL
                    $this->createOrderResult[$orderId] = [
                        'success' => (empty($response->Error) && $response->ErrorLevel == 0),
                        'message' => (empty($response->Error) && $response->ErrorLevel == 0
                            ? 'Shipment successfully registered'
                            : 'Code: ' . $response->ErrorLevel . ': ' . $response->Error),
                        'newPackagenumber' => false,
                        'packages' => $shipmentItems,
                    ];

                    // saves shipping information
                    $this->shippingInformationRepositoryContract->saveShippingInformation(
                        [
                            'orderId' => $orderId,
                            'transactionId' => $response->Shipment->TrackingNumber,
                            'shippingServiceProvider' => $this->services[$service],
                            'shippingStatus' => 'registered',
                            'shippingCosts' => 0.00,
                            'additionalData' => $shipmentItems,
                            'registrationAt' => date(\DateTime::W3C),
                            'shipmentAt' => date(\DateTime::W3C, strtotime($shipmentDate))
                        ]
                    );
                } catch (\SoapFault $soapFault) {
                    DebuggerCurl::debug(array(
                        'Catch Error' => $soapFault->getMessage(),
                    ));
                }

            }

        }

        // return all results to service
        return $this->createOrderResult;
    }

    /**
     * Cancels registered shipment(s)
     *
     * @param Request $request
     * @param array $orderIds
     *
     * @return array
     */
    public function deleteShipments(Request $request, $orderIds): array
    {
        DebuggerCurl::debug(array('place' => 'deleteShipments'));
        $orderIds = $this->getOrderIds($request, $orderIds);

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            if (isset($shippingInformation->additionalData) && is_array($shippingInformation->additionalData)) {
                foreach ($shippingInformation->additionalData as $additionalData) {
                    try {
                        $shipmentNumber = $additionalData['shipmentNumber'];

                        // use the shipping service provider's API here
                        $response = '';

                        $this->createOrderResult[$orderId] = $this->buildResultArray(
                            true,
                            $this->getStatusMessage($response),
                            false,
                            null);

                    } catch (\SoapFault $soapFault) {
                        // exception handling
                        DebuggerCurl::debug($soapFault->getMessage());
                    }
                }

                // resets the shipping information of current order
                $this->shippingInformationRepositoryContract->resetShippingInformation($orderId);
            }
        }

        // return result array
        return $this->createOrderResult;
    }

    /**
     * Returns the parcel service preset for the given Id.
     *
     * @param int $parcelServicePresetId
     *
     * @return ParcelServicePreset
     */
    private function getParcelServicePreset($parcelServicePresetId)
    {
        /** @var ParcelServicePresetRepositoryContract $parcelServicePresetRepository */
        $parcelServicePresetRepository = pluginApp(ParcelServicePresetRepositoryContract::class);

        $parcelServicePreset = $parcelServicePresetRepository->getPresetById($parcelServicePresetId);

        if ($parcelServicePreset) {
            return $parcelServicePreset;
        } else {
            return null;
        }
    }

    /**
     * Returns a formatted status message
     *
     * @param array $response
     *
     * @return string
     */
    private function getStatusMessage($response): string
    {
        return 'Code: ' . $response['status']; // should contain error code and descriptive part
    }

    /**
     * Returns all order ids with shipping status 'open'
     *
     * @param array $orderIds
     *
     * @return array
     */
    private function getOpenOrderIds($orderIds): array
    {
        $openOrderIds = array();

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            if ($shippingInformation->shippingStatus == null || $shippingInformation->shippingStatus == 'open') {
                $openOrderIds[] = $orderId;
            }
        }

        return $openOrderIds;
    }


    /**
     * Returns an array in the structure demanded by plenty service
     *
     * @param bool $success
     * @param string $statusMessage
     * @param bool $newShippingPackage
     * @param array $shipmentItems
     *
     * @return array
     */
    private function buildResultArray($success = false, $statusMessage = '', $newShippingPackage = false, $shipmentItems = []): array
    {
        return [
            'success' => $success,
            'message' => $statusMessage,
            'newPackagenumber' => $newShippingPackage,
            'packages' => $shipmentItems,
        ];
    }

    /**
     * Returns shipment array
     *
     * @param string $labelUrl
     * @param string $shipmentNumber
     *
     * @return array
     */
    private function buildShipmentItems($labelUrl, $shipmentNumber): array
    {
        return [
            'labelUrl' => $labelUrl,
            'shipmentNumber' => $shipmentNumber,
        ];
    }

    /**
     * Returns package info
     *
     * @param string $packageNumber
     * @param string $labelUrl
     *
     * @return array
     */
    private function buildPackageInfo($packageNumber, $labelUrl): array
    {
        return [
            'packageNumber' => $packageNumber,
            'label' => $labelUrl
        ];
    }

    /**
     * Returns all order ids from request object
     *
     * @param Request $request
     * @param         $orderIds
     *
     * @return array
     */
    private function getOrderIds(Request $request, $orderIds): array
    {
        if (is_numeric($orderIds)) {
            $orderIds = array($orderIds);
        } else if (!is_array($orderIds)) {
            $orderIds = $request->get('orderIds');
        }

        return $orderIds;
    }

    /**
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function getLabels(Request $request, $orderIds = array()): array
    {
        $orderIds = $this->getOrderIds($request, $orderIds);
        $labels = array();
        DebuggerCurl::debug(array(
            'place' => 'getLabels',
        ));

        foreach ($orderIds as $orderId) {
            /** @var OrderShippingPackage $packages */
            $packages = $this->orderShippingPackage->listOrderShippingPackages($orderId);

            foreach ($packages as $package) {
                DebuggerCurl::debug(array('paket' => $package));
                $labelKey = explode('/', $package->labelPath)[1];

                if ($this->storageRepository->doesObjectExist('ShippingTutorial', $labelKey)) {
                    $storageObject = $this->storageRepository->getObject('ShippingTutorial', $labelKey);
                    DebuggerCurl::debug(array('storageObj' => $storageObject));
                    $labels[] = $storageObject->body;
                }
            }
        }

        return $labels;
    }

}
