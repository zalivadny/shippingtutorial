<?php

namespace ShippingTutorial\Controllers;

use Plenty\Modules\Account\Address\Contracts\AddressRepositoryContract;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Order\Shipping\PackageType\Contracts\ShippingPackageTypeRepositoryContract;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\ConfigRepository;

class ShippingControllerPptr extends ShippingControllerBase
{
    /**
     * ShippingControllerUntr constructor.
     * @param Request $request
     * @param OrderRepositoryContract $orderRepository
     * @param AddressRepositoryContract $addressRepositoryContract
     * @param OrderShippingPackageRepositoryContract $orderShippingPackage
     * @param StorageRepositoryContract $storageRepository
     * @param ShippingInformationRepositoryContract $shippingInformationRepositoryContract
     * @param ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract
     * @param ConfigRepository $config
     */
    public function __construct(Request $request,
                                OrderRepositoryContract $orderRepository,
                                AddressRepositoryContract $addressRepositoryContract,
                                OrderShippingPackageRepositoryContract $orderShippingPackage,
                                StorageRepositoryContract $storageRepository,
                                ShippingInformationRepositoryContract $shippingInformationRepositoryContract,
                                ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract,
                                ConfigRepository $config)
    {
        parent::__construct($request,
            $orderRepository,
            $addressRepositoryContract,
            $orderShippingPackage,
            $storageRepository,
            $shippingInformationRepositoryContract,
            $shippingPackageTypeRepositoryContract,
            $config);
    }

    /**
     * Registers shipment(s)
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function registerShipments(Request $request, $orderIds = []): array
    {
        return $this->registerShipmentsBase($request, $orderIds, 'PPTR');
    }

}
