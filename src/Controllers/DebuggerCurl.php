<?php


namespace ShippingTutorial\Controllers;

/**
 * Class DebuggerCurl
 * @package ShippingTutorial\Controllers
 */
class DebuggerCurl
{
    /**
     * @param mixed $data
     * @return void
     */
    public static function debug($data = false): void
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(array('some-paranoia' => 'no', 'data' =>
                json_encode($data))));
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }

}