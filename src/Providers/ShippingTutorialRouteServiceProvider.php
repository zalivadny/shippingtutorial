<?php
namespace ShippingTutorial\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

/**
 * Class ShippingTutorialRouteServiceProvider
 * @package ShippingTutorial\Providers
 */
class ShippingTutorialRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param Router    $router
     */
    public function map(Router $router): void
    {
        $router->get('label', 'ShippingTutorial\Controllers\TemplateController@label');
  	}

}
