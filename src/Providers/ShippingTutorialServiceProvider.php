<?php

namespace ShippingTutorial\Providers;

use Plenty\Modules\Order\Shipping\ServiceProvider\Services\ShippingServiceProviderService;
use Plenty\Plugin\ServiceProvider;

/**
 * Class ShippingTutorialServiceProvider
 * @package ShippingTutorial\Providers
 */
class ShippingTutorialServiceProvider extends ServiceProvider
{
    /** Register the service provider. */
    public function register(): void
    {
        // add REST routes by registering a RouteServiceProvider if necessary
        $this->getApplication()->register(ShippingTutorialRouteServiceProvider::class);
    }

    /**
     * @param ShippingServiceProviderService $shippingServiceProviderService
     */
    public function boot(ShippingServiceProviderService $shippingServiceProviderService): void
    {
        $shippingServiceProviderService->registerShippingProvider(
            'UNTR',
            [
                'de' => 'SpringGDS UNTRACKED',
                'en' => 'SpringGDS UNTRACKED'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerUntr@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerUntr@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerUntr@deleteShipments',
            ]
        );

        $shippingServiceProviderService->registerShippingProvider(
            'TRCK',
            [
                'de' => 'SpringGDS TRACKED',
                'en' => 'SpringGDS TRACKED'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerTrck@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerTrck@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerTrck@deleteShipments',
            ]
        );

        $shippingServiceProviderService->registerShippingProvider(
            'PPBNT',
            [
                'de' => 'SpringGDS Packet Boxable Non Tracked',
                'en' => 'SpringGDS Packet Boxable Non Tracked'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerPpbnt@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerPpbnt@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerPpbnt@deleteShipments',
            ]
        );

        $shippingServiceProviderService->registerShippingProvider(
            'PPTR',
            [
                'de' => 'SpringGDS Packet Registered',
                'en' => 'SpringGDS Packet Registered'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerPptr@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerPptr@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerPptr@deleteShipments',
            ]
        );

        $shippingServiceProviderService->registerShippingProvider(
            'PPTT',
            [
                'de' => 'SpringGDS Packet Tracked',
                'en' => 'SpringGDS Packet Tracked'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerPptt@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerPptt@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerPptt@deleteShipments',
            ]
        );

        $shippingServiceProviderService->registerShippingProvider(
            'PPBTT',
            [
                'de' => 'SpringGDS Packet Boxable Tracked',
                'en' => 'SpringGDS Packet Boxable Tracked'
            ],
            [
                'ShippingTutorial\\Controllers\\ShippingControllerPpbtt@registerShipments',
                'ShippingTutorial\\Controllers\\ShippingControllerPpbtt@getLabels',
                'ShippingTutorial\\Controllers\\ShippingControllerPpbtt@deleteShipments',
            ]
        );

    }

}
